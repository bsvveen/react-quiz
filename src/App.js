import React from 'react';
import Quiz from "./Components/Quiz";

class App extends React.Component {

  state = { QuizSource :  undefined };

  componentDidMount() {    
    var QuizSource = new URLSearchParams( window.location.search).get('src');
    this.setState({ QuizSource: QuizSource });      
  }
  
  render() {   
    if (!this.state.QuizSource)
      return <div>Please provide a quiz source</div>;

    return (
      <div id="App"> 
        <div className="window">    
          <div className="body" >
              <Quiz quizSource={this.state.QuizSource} />   
          </div>
          <div className="footer"></div>
        </div> 
        <img className="vogelhuis" src="https://ecare.nl/wp-content/themes/ecare/img/vogelhuis.svg"></img>
        <img className="huisjes" src="https://ecare.nl/wp-content/themes/ecare/img/huisjes-menu.svg"></img>      
      </div>
    );
  }

}

export default App;