export const locale =  {
    "landingHeaderText": "<questionLength> Vragen",
    "question": "Vraag: ",
    "messageCorrectAnswer": "Helemaal correct, u mag door naar de volgende vraag.",
    "messageInCorrectAnswer": "Helaas u heeft de vraag niet correct beantwoord.",
    "messageTheCorrectAnswerIs": "Het juiste antwoord is: ",
    "messageYourAnswers": "De door u gekozen antwoorden: ",
    "startQuizBtn": "Start Quiz",
    "resultFilterAll": "Allemaal",
    "resultFilterCorrect": "Goed",
    "resultFilterIncorrect": "Fout",
    "nextQuestionBtn": "Volgende vraag",
    "resultPageHeaderText": "Je hebt alle vragen beantwoord. Je hebt <correctIndexLength> van de <questionLength> vragen goed."
  } 