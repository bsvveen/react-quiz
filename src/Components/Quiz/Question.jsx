import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { locale } from './Locale';

export default class Question extends Component {  

  static propTypes = {
    question: PropTypes.PropTypes.shape({
      question: PropTypes.string.isRequired,     
      answers:  PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string.isRequired,
        isCorrect: PropTypes.bool,
        hasbeenChosen: PropTypes.bool
      })).isRequired,  
      explanation: PropTypes.string
    }).isRequired,   
    submitAnswer: PropTypes.func.isRequired,
    nextQuestion: PropTypes.func.isRequired 
  }

  state = { 
    "answerIscorrect" : undefined,
    "answers" : []
  };

  submitMultipleAnswers = (event) => {    
    event.preventDefault();

    let correctAnswers = this.props.question.answers.reduce((acc, cur, index) => {
      if (cur.isCorrect)
        acc.push(index);
      return acc;
    }, []); 
    let answerIscorrect = correctAnswers.every(ca => this.state.answers.find(a => a === ca));   
    this.setState({ answerIscorrect : answerIscorrect}, () => {
      this.props.submitAnswer(this.state.answers);
    })  
  }

  handleChange = (event) => {
    let answersCopy = Object.assign([], this.state.answers);    
    
    if (event.target.checked && this.state.answers.indexOf(event.target.value) === -1)
      answersCopy.push(parseInt(event.target.value));

    if (!event.target.checked && this.state.answers.indexOf(parseInt(event.target.value)) >= 0)
      answersCopy.filter( a => a !== event.target.value);

    this.setState({ answers: answersCopy })
  }

  submitSingleAnswer = (givenAnswerIndex) => {   
    this.setState({ answerIscorrect : this.props.question.answers[givenAnswerIndex-1].isCorrect}, () => {
      this.props.submitAnswer([givenAnswerIndex-1]);
    }) 
  }   

  nextQuestion = () => {    
    this.props.nextQuestion();     
  }   

  render() {
    let question = this.props.question;   
    let hasmultipleAnswers = (question.answers.filter(a => a.isCorrect).length > 1)
    
    return ( 
      <div className="container">
        { this.state.answerIscorrect === undefined &&
          <div className="container">
            <div className="container">{question.question}</div> 
            <form onSubmit={this.submitMultipleAnswers}> 
            { question.answers.map( (answer, index) => {
              return (
                <div key={index} className="row">
                  <div className="col-1">
                    {hasmultipleAnswers?
                    <input type="checkbox" value={index} onChange={this.handleChange} />:
                    <button onClick={() => this.submitSingleAnswer(index+1)}>{String.fromCharCode(65 + index)}</button>
                    }
                  </div><div className="col-9">  
                    {answer.text}
                  </div>
                </div>
              )            
            })} 
            {hasmultipleAnswers && <button type="submit">Submit</button>}
           </form> 
          </div>        
        }

        { this.state.answerIscorrect !== undefined &&
          <div className="container">

            <div className="alert">
              { this.state.answerIscorrect && locale.messageCorrectAnswer } 
              { !this.state.answerIscorrect && locale.messageInCorrectAnswer }               
            </div>

            { !this.state.answerIscorrect && 
              <div className="container"> 
                <strong>{ locale.messageTheCorrectAnswerIs }</strong>
                <ul>
                { question.answers.filter(a => a.isCorrect).map((a2, index) => {
                  return <li key={index}> {a2.text} </li>
                })}
                </ul>
              </div>
            } 

            <div className="container">{ question.explanation }</div> 

            <div className="container">
              <button onClick={() => this.nextQuestion(this.state.currentQuestionIndex)}>{locale.nextQuestionBtn}</button>
            </div>
           
          </div> 
        } 
      </div>  
    );
  }
}