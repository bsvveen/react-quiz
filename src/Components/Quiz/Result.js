import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { locale } from './Locale';

export default class QuizReults extends Component {

  static propTypes = {
    quiz: PropTypes.object.isRequired
  };      

  render() {    ;
    let questions = this.props.quiz.questions
    let numberOfQuestionsCorrect = questions.reduce((cumm, curr) => {
      if (curr.answers.filter((a) => a.isCorrect).every(a => a.hasBeenChosen))
        cumm++
      return cumm
    }, 0)

    return (<div>  
      <div className="container title">{locale.resultPageHeaderText.replace("<correctIndexLength>", numberOfQuestionsCorrect).replace("<questionLength>", questions.length)}</div>
      <div className="container">{
        questions.map((question, index) => {
          let correctAnswers = question.answers.filter((a) => a.isCorrect);
          let choosenAnswers = question.answers.filter(a => a.hasBeenChosen);
          let questionIsCorrect = correctAnswers.every(a => a.hasBeenChosen);
          return (
            <div className="container">
              <div className="container title">{locale.question + (index+1)}</div>
              <div className="container">{question.question}</div> 
              <div className="container">
                <strong>{locale.messageYourAnswers}</strong>              
                <div>{choosenAnswers.map((a) => { return <div>- {a.text}</div>})}</div> 
              </div>  
              <div className="alert">{(questionIsCorrect)?locale.messageCorrectAnswer:locale.messageInCorrectAnswer}</div>  
              {!questionIsCorrect&&
              <div className="container">
                <strong>{locale.messageTheCorrectAnswerIs}</strong>              
                <div>{correctAnswers.map((a) => { return <div>- {a.text}</div>})}</div> 
              </div>  
              }
              <div className="container">{question.explanation}</div>
            </div>
          )
        })            
      }</div>  
    </div>   ) 
  }
}
