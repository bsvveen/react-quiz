import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Question from './Question';
import QuizResult from './Result';
import { locale } from './Locale';

export default class Quiz extends Component {

  static propTypes = {
    quizSource: PropTypes.string.isRequired
  }; 

  state = { 
    quiz: undefined, 
    start: false,
    end: false,
    currentQuestionIndex: undefined
  }; 

  componentDidMount() {      
    fetch('QuizSources/' + this.props.quizSource + ".json").then( 
      (response) => { return response.json() }
    ).then( 
      (json) => { this.setState({ quiz: json, currentQuestionIndex : 0 })}
    )   
  }

  start = () => { this.setState({ start: true })}

  onSubmitAnswer = (answerIndexes) => { 
    let quizCopy = Object.assign({},this.state.quiz);
    answerIndexes.forEach(answerIndex => {
      quizCopy.questions[this.state.currentQuestionIndex].answers[answerIndex].hasBeenChosen = true });
    this.setState({ quiz: quizCopy })
  }

  onNextQuestion = () => { 
    if ( this.state.currentQuestionIndex + 1 === this.state.quiz.questions.length) {
      this.setState({ end: true }) 
    } else {
      this.setState(prevState => { return { currentQuestionIndex: prevState.currentQuestionIndex + 1 }})
    }
  } 

  render() {      
    let quiz = this.state.quiz;

    if (!quiz) 
      return <div>Error Loading Quiz from {this.props.quizSource}</div>

    return (
      <div className="quizcontainer">
        { !this.state.start &&
          <div className="container">
            <h2>{quiz.title}</h2>
            <div>{locale.landingHeaderText.replace("<questionLength>" , quiz.questions.length )}</div><br />
              { quiz.summary && <div>{quiz.summary}</div> }
            <div><br />
              <button onClick={() => this.start()} className="startQuizBtn btn">{locale.startQuizBtn}</button>
            </div>
          </div>
        }

        { this.state.start && !this.state.end && 
          <div className="container">
            <div className="title">{locale.question} {this.state.currentQuestionIndex + 1} van {quiz.questions.length}</div>
            <Question 
              key = { this.state.currentQuestionIndex }
              question={ quiz.questions[this.state.currentQuestionIndex] }             
              submitAnswer = { this.onSubmitAnswer }
              nextQuestion = { this.onNextQuestion }
            />
          </div>
        }

        { this.state.end && <QuizResult quiz={this.state.quiz} /> }
      </div>
    );
  }
}